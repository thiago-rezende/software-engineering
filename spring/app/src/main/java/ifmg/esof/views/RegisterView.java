package ifmg.esof.views;

import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.formlayout.FormLayout.ResponsiveStep;
import com.vaadin.flow.component.page.BodySize;
import com.vaadin.flow.component.page.Viewport;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.router.Route;
import ifmg.esof.model.User;
import ifmg.esof.repository.UserRepository;
import ifmg.esof.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;

@Route("/register")
@Viewport("width=device-width, minimum-scale=1.0, initial-scale=1.0, user-scalable=yes")
@BodySize(height = "100vh", width = "100vw")
public class RegisterView extends VerticalLayout {

    @Autowired
    private UserService userService;

          public RegisterView() {
                FormLayout columnLayout = new FormLayout();
                columnLayout.setSizeFull();

                TextField email = new TextField();
                email.setLabel("Email");
                email.setPlaceholder("Digite seu email...");

                PasswordField password = new PasswordField();
                password.setLabel("Senha");
                password.setPlaceholder("Digite sua senha...");

                columnLayout.add(email, password);

                Button register = new Button("Registrar", event -> {
                    String emailValue = email.getValue();
                    String passwordValue = password.getValue();

                    User user = new User(emailValue, passwordValue);

                    userService.createUser(user);

                    /* don't do this in production */
                    UI.getCurrent().getPage().executeJavaScript("localStorage.setItem($0, $1)", "id", String.valueOf(user.getId()));
                    UI.getCurrent().getPage().executeJavaScript("localStorage.setItem($0, $1)", "email", user.getEmail());
                    UI.getCurrent().getPage().executeJavaScript("localStorage.setItem($0, $1)", "password", user.getPassword());

                    UI.getCurrent().navigate(DashboardView.class);
                });
                register.getElement().setAttribute("aria-label", "Registrar Conta");

                setSizeFull();
                setJustifyContentMode(JustifyContentMode.CENTER);
                setDefaultHorizontalComponentAlignment(Alignment.CENTER);

                add(
                      new H1("ESOF II"),
                      new HorizontalLayout(
                        columnLayout
                      ),
                      new HorizontalLayout(
                        register
                      )
                );
      }
}
