package ifmg.esof.services;

import ifmg.esof.model.Order;
import ifmg.esof.model.Product;
import ifmg.esof.model.User;
import ifmg.esof.repository.OrderRepository;
import ifmg.esof.repository.ProductRepository;
import ifmg.esof.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private OrderRepository orderRepository;

    public Long createProduct(Product product, Long id) {
        Order _order = orderRepository.getById(id);

        Product _product = productRepository.save(product);
        _product.getOrders().add(_order);

        return _product.getId();
    }

    public void updateProduct(Product product) {
        Product _product = productRepository.getById(product.getId());

        _product.setName(product.getName());
        _product.setPrice(product.getPrice());
        _product.setQuantity(product.getQuantity());

        productRepository.save(_product);
    }

    public void deleteProduct(Long id) {
        Product _product = productRepository.getById(id);

        productRepository.delete(_product);
    }

    public void deleteProduct(Product product) {
        productRepository.delete(product);
    }

    public User getProduct(Long id) {
        return userRepository.getById(id);
    }
}
